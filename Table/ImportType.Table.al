table 5563319 "mse365 Import Type"
{
    Caption = 'Import Type';
    DataClassification = CustomerContent;
    LookupPageId = "mse365 Import Types";
    DrillDownPageId = "mse365 Import Types";

    fields
    {
        field(1; "Code"; Code[10])
        {
            Caption = 'Code';
            DataClassification = CustomerContent;
            NotBlank = true;
            Editable = false;
        }
        field(10; Description; Text[100])
        {
            Caption = 'Description';
            DataClassification = CustomerContent;
        }
        field(20; "Buffer Table No."; Integer)
        {
            Caption = 'Buffer Table No.';
            DataClassification = CustomerContent;
            NotBlank = true;
            Editable = false;
            TableRelation = AllObjWithCaption."Object ID" where("Object Type" = const(Table));
        }
        field(21; "Buffer Table Description"; Text[50])
        {
            Caption = 'Buffer Table Description';
            FieldClass = FlowField;
            CalcFormula = lookup (AllObjWithCaption."Object Caption" where("Object Type" = const(Table), "Object ID" = field("Buffer Table No.")));
            Editable = false;
        }
        field(30; "Processing Codeunit Id"; Integer)
        {
            Caption = 'Processing Codeunit Id';
            DataClassification = CustomerContent;
            NotBlank = true;
            TableRelation = AllObjWithCaption."Object ID" where("Object Type" = const(Codeunit));
            Editable = false;
        }
        field(31; "Proc. Codeunit Desc."; Text[50])
        {
            Caption = 'Processing Codeunit Description';
            FieldClass = FlowField;
            CalcFormula = lookup (AllObjWithCaption."Object Caption" where("Object Type" = const(Codeunit), "Object ID" = field("Processing Codeunit Id")));
            Editable = false;
        }
        field(50; "Remove First Line"; Boolean)
        {
            Caption = 'Remove First Line';
            DataClassification = CustomerContent;
        }

    }
    keys
    {
        key(PK; Code)
        {
            Clustered = true;
        }
        key(ProcessingCodeunitId; "Processing Codeunit Id")
        {
        }
    }

    procedure CreateNew(TypeCode: Code[10]; ImportDescription: Text; BufferTableNo: Integer; ProcessingCodeunitId: Integer; RemoveFirstLine: Boolean)
    begin
        if Get(TypeCode) then
            exit;

        Init();
        Validate(Code, TypeCode);
        Validate(Description, ImportDescription);
        Validate("Buffer Table No.", BufferTableNo);
        validate("Processing Codeunit Id", ProcessingCodeunitId);
        Validate("Remove First Line", RemoveFirstLine);
        Insert(true);
    end;
}
