table 5563318 "mse365 Import Setup"
{
    Caption = 'Import Setup';
    DataClassification = CustomerContent;

    fields
    {
        field(1; "Primary Key"; Code[10])
        {
            Caption = 'Primary Key';
            DataClassification = CustomerContent;
        }
        field(10; "Import Nos."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Import Nos.';
            TableRelation = "No. Series";
        }
    }
    keys
    {
        key(PK; "Primary Key")
        {
            Clustered = true;
        }
    }
}
