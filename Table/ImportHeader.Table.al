table 5563316 "mse365 Import Header"
{
    Caption = 'mse365 Import Header';
    LookupPageId = "mse365 Import List";
    DrillDownPageId = "mse365 Import List";
    fields
    {
        field(1; "No."; Code[20])
        {
            Caption = 'No.';
            DataClassification = CustomerContent;
        }
        field(10; "Created DateTime"; DateTime)
        {
            Caption = 'Created DateTime';
            Editable = false;
            DataClassification = CustomerContent;
        }
        field(11; "User ID"; Code[50])
        {
            Caption = 'User ID';
            Editable = false;
            TableRelation = User."User Name";
            ValidateTableRelation = false;
            DataClassification = CustomerContent;

            trigger OnLookup()
            var
                UserMgt: Codeunit "User Management";
            begin
                UserMgt.LookupUserID("User ID");
            end;
        }
        field(12; "Import Type Code"; Code[10])
        {
            Caption = 'Import Type Code';
            DataClassification = CustomerContent;
            TableRelation = "mse365 Import Type";
            NotBlank = true;
        }
        field(15; Status; Enum "mse365 Import Status")
        {
            DataClassification = CustomerContent;
            Caption = 'Status';
            Editable = false;
        }
        field(16; "Task Scheduler Status"; Option)
        {
            DataClassification = CustomerContent;
            Caption = 'Task Scheduler Status';
            Editable = false;
            OptionMembers = " ",Running,Error,Done;
            OptionCaption = ' ,Running,Error,Done';
        }
        field(17; "Task Scheduler Text"; Text[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Task Scheduler Text';
            Editable = false;
        }
        field(20; Filename; Text[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Filename';
            Editable = false;
        }
        field(21; "delete spaces"; Boolean)
        {
            Caption = 'Delete spaces';
            DataClassification = CustomerContent;
        }
        field(25; "File Blob"; Blob)
        {
            Caption = 'File Blob';
            Subtype = UserDefined;
            DataClassification = CustomerContent;
        }
        field(30; "Excel Sheet Name"; Text[30])
        {
            DataClassification = CustomerContent;
            Caption = 'Excel Sheet Name';
            Editable = false;
        }
        field(50; "No. Of Imported Lines"; Integer)
        {
            CalcFormula = Count ("mse365 Import Line" Where("mse365 Import No." = Field("No.")));
            Caption = 'No. Of Imported Lines';
            Editable = false;
            FieldClass = FlowField;
        }
        field(51; "No. Of Finished Lines"; Integer)
        {
            CalcFormula = Count ("mse365 Import Line" Where("mse365 Import No." = Field("No."), "Import Status" = Const(Finished)));
            Caption = 'No. Of Finished Lines';
            Editable = false;
            FieldClass = FlowField;
        }
        field(52; "No. Of Erroneous Lines"; Integer)
        {
            CalcFormula = Count ("mse365 Import Line" Where("mse365 Import No." = Field("No."), "Import Status" = Const(Error)));
            Caption = 'No. Of Erroneous Lines';
            Editable = false;
            FieldClass = FlowField;
        }
        field(53; "No. Of Warning Lines"; Integer)
        {
            CalcFormula = Count("mse365 Import Line" Where("mse365 Import No." = Field("No."), "No. Of Warnings/Info" = filter(> 0)));
            Caption = 'No. Of Warning Lines';
            Editable = false;
            FieldClass = FlowField;
        }
        field(60; "No. Of Import Errors"; Integer)
        {
            CalcFormula = count ("mse365 Imp. Setup Error" where("Import No." = field("No.")));
            Caption = 'No. Of Finished Lines';
            Editable = false;
            FieldClass = FlowField;
        }
    }

    keys
    {
        key(Key1; "No.")
        {
            Clustered = true;
        }
    }

    trigger OnDelete()
    begin
        DeleteRelatedEntries();
    end;

    trigger OnInsert()
    begin
        ImportSetup.Get();

        if "No." = '' then begin
            ImportSetup.TestField("Import Nos.");
            NoSeriesMgt.InitSeries(ImportSetup."Import Nos.", ImportSetup."Import Nos.", WorkDate(), "No.", ImportSetup."Import Nos.");
        end;

        "Created DateTime" := CurrentDateTime;
        "User ID" := CopyStr(UserId(), 1, MaxStrLen("User ID"));
    end;

    procedure ImportFromExcel()
    begin
        ImportMgt.HeaderImportFromExcel(Rec);
    end;

    procedure CheckAllLines()
    begin
        ImportMgt.HeaderCheckAllLines(Rec);
    end;

    procedure TransferAllLines()
    begin
        ImportMgt.HeaderTransferAllLines(Rec);
    end;

    procedure UpdateStatus()
    begin
        ImportMgt.HeaderUpdateStatus(Rec);
    end;

    procedure ResetHeader()
    begin
        ImportMgt.HeaderResetHeader(Rec);
    end;

    local procedure DeleteRelatedEntries()
    begin
        ImportMgt.HeaderDeleteRelatedEntries(Rec);
    end;

    var
        ImportSetup: Record "mse365 Import Setup";
        NoSeriesMgt: Codeunit NoSeriesManagement;
        ImportMgt: Codeunit "mse365 Import Mgt.";
}

