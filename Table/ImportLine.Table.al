table 5563317 "mse365 Import Line"
{
    Caption = 'mse365 Import Line';

    fields
    {
        field(1; "mse365 Import No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'mse365 Import No.';
            TableRelation = "mse365 Import Header";
        }
        field(2; "mse365 Import Line No."; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'mse365 Import Line No.';
        }
        field(5; "Check Only"; Boolean)
        {
            DataClassification = CustomerContent;
            Editable = false;
        }
        field(6; "Import Type Code"; Code[10])
        {
            Caption = 'Import Type Code';
            TableRelation = "mse365 Import Type";
            FieldClass = FlowField;
            CalcFormula = lookup ("mse365 Import Header"."Import Type Code" where("No." = field("mse365 Import No.")));
            Editable = false;
        }
        field(7; "First Error Text"; Text[250])
        {
            Caption = 'First Error Text';
            TableRelation = "mse365 Import Type";
            FieldClass = FlowField;
            CalcFormula = lookup ("mse365 Imp. Setup Error"."Error Text 1" where("Import No." = field("mse365 Import No."), "Import Line No." = field("mse365 Import Line No.")));
            Editable = false;
        }
        field(11; "Text Value 1"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(11);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;

        }
        field(12; "Text Value 2"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(12);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(13; "Text Value 3"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(13);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(14; "Text Value 4"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(14);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(15; "Text Value 5"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(15);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(16; "Text Value 6"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(16);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(17; "Text Value 7"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(17);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(18; "Text Value 8"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(18);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(19; "Text Value 9"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(19);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(20; "Text Value 10"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(20);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(21; "Text Value 11"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(21);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(22; "Text Value 12"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(22);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }

        field(23; "Text Value 13"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(23);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(24; "Text Value 14"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(24);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(25; "Text Value 15"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(25);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(26; "Text Value 16"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(26);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(27; "Text Value 17"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(27);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(28; "Text Value 18"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(28);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(29; "Text Value 19"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(29);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(30; "Text Value 20"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(30);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(31; "Text Value 21"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(31);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(32; "Text Value 22"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(32);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(33; "Text Value 23"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(33);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(34; "Text Value 24"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(34);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(35; "Text Value 25"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(35);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(36; "Text Value 26"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(36);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(37; "Text Value 27"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(37);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(38; "Text Value 28"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(38);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(39; "Text Value 29"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(39);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(40; "Text Value 30"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(40);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(41; "Text Value 31"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(41);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(42; "Text Value 32"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(42);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(43; "Text Value 33"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(43);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(44; "Text Value 34"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(44);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(45; "Text Value 35"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(45);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(47; "Text Value 36"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(47);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(48; "Text Value 37"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(48);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(49; "Text Value 38"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(49);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(50; "Text Value 39"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(50);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }
        field(51; "Text Value 40"; Text[250])
        {
            DataClassification = CustomerContent;
            CaptionClass = GetCurrentFieldCaption(51);

            trigger OnValidate()
            begin
                ValidateImportedField();
            end;
        }


        field(100; "No. Of Errors"; Integer)
        {
            CalcFormula = Count ("mse365 Imp. Setup Error" Where("Import No." = Field("mse365 Import No."),
                                                                       "Import Line No." = Field("mse365 Import Line No."),
                                                                       "Error Type" = Const(Error)));
            Caption = 'No. Of Errors';
            Editable = false;
            FieldClass = FlowField;
        }
        Field(101; "No. Of Warnings/Info"; Integer)
        {
            CalcFormula = Count ("mse365 Imp. Setup Error" Where("Import No." = Field("mse365 Import No."),
                                                                       "Import Line No." = Field("mse365 Import Line No."),
                                                                       "Error Type" = Filter(Info | Warning)));
            Caption = 'No. Of Warnings/Info';
            Editable = false;
            FieldClass = FlowField;
        }
        field(1000; "Import Status"; Option)
        {
            DataClassification = CustomerContent;
            Caption = 'Import Status';
            Editable = false;
            OptionCaption = ' ,Imported,Error,Updated,OK,Finished';
            OptionMembers = " ",Imported,Error,Updated,OK,Finished;
        }
    }

    keys
    {
        key(Key1; "mse365 Import No.", "mse365 Import Line No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }

    trigger OnModify()
    begin
        Check();
    end;

    trigger OnDelete()
    begin
        ClearImportSetupErrors();
    end;

    local procedure ValidateImportedField()
    begin
        mse365ImportMgt.LineValidateImportedField(Rec);
    end;

    procedure Check()
    begin
        mse365ImportMgt.LineCheck(Rec);
    end;

    procedure AddImportSetupError(ErrorText: Text; ErrorRecID: Variant; PageID: Integer; ErrorCode: Integer; ImportErrorType: Enum "mse365 Import Error Type")
    begin
        ContribImpSetupErrMgt.LineAddImportSetupError(Rec, ErrorText, ErrorRecID, PageID, ErrorCode, ImportErrorType);
    end;

    procedure ClearImportSetupErrors()
    begin
        ContribImpSetupErrMgt.LineClearImportSetupErrors(Rec);
    end;

    procedure HasErrors(): Boolean
    begin
        exit(ContribImpSetupErrMgt.LineHasErrors(Rec));
    end;

    procedure GetCurrentFieldCaption(ForFieldNo: Integer): Text
    begin
        exit(mse365ImportMgt.LineGetCurrentFieldCaption(Rec, ForFieldNo));
    end;

    var
        mse365ImportMgt: Codeunit "mse365 Import Mgt.";
        ContribImpSetupErrMgt: Codeunit "mse365 Imp. Setup Err. Mgt.";
}

