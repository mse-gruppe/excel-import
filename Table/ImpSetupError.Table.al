table 5563320 "mse365 Imp. Setup Error"
{
    Caption = 'mse365 Setup Error';
    DrillDownPageID = "mse365 Imp. Setup Errors";
    LookupPageID = "mse365 Imp. Setup Errors";

    fields
    {
        field(1; "Import No."; Code[20])
        {
            DataClassification = CustomerContent;
            Caption = 'Import No.';
            NotBlank = true;
            TableRelation = "mse365 Import Header";
        }
        field(2; "Import Line No."; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Import Line No.';
        }
        field(3; "Entry No."; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Entry No.';
        }
        field(10; "Error Text 1"; Text[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Error Text 1';
        }
        field(11; "Error Text 2"; Text[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Error Text 2';
        }
        field(12; "Error Text 3"; Text[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Error Text 3';
        }
        field(13; "Error Text 4"; Text[250])
        {
            DataClassification = CustomerContent;
            Caption = 'Error Text 4';
        }
        field(20; "Error RecordID"; RecordID)
        {
            DataClassification = CustomerContent;
            Caption = 'Error RecordID';
        }
        field(21; "Page ID"; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Page ID';
        }
        field(50; "Error Type"; Enum "mse365 Import Error Type")
        {
            DataClassification = CustomerContent;
            Caption = 'Error Type';
        }
        field(51; "Error Code"; Integer)
        {
            DataClassification = CustomerContent;
            Caption = 'Error Code';
        }
    }

    keys
    {
        key(Key1; "Import No.", "Import Line No.", "Entry No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }

    trigger OnInsert()
    begin
        GetNextEntryNo();
    end;

    var
        ContribImpSetupErrMgt: Codeunit "mse365 Imp. Setup Err. Mgt.";

    local procedure GetNextEntryNo()
    begin
        "Entry No." := ContribImpSetupErrMgt.GetNextEntryNo(Rec);
    end;

    procedure GetDataCaptionExpression(): Text
    begin
        exit(ContribImpSetupErrMgt.GetDataCaptionExpression(Rec));
    end;

    procedure SetErrorText(NewErrorText: Text)
    begin
        ContribImpSetupErrMgt.SetErrorText(Rec, NewErrorText);
    end;

    procedure GetErrorText(): Text
    begin
        exit(ContribImpSetupErrMgt.GetErrorText(Rec));
    end;

    procedure OpenPageForErrorRecord()
    begin
        ContribImpSetupErrMgt.OpenPageForErrorRecord(Rec);
    end;
}

