page 5563313 "mse365 Import DropZone Part"
{
    Caption = 'Excel Import';
    PageType = ListPart;
    Editable = true;

    layout
    {
        area(Content)
        {
            usercontrol(DropZone; "mse365 DropZone")
            {
                ApplicationArea = All;

                trigger ControlAddInReady()
                begin
                    AddinReady := true;
                    CurrPage.DropZone.showExcelIcon();
                end;

                trigger DataReceived(Data: Text; Filename: Text)
                begin
                    DropZoneImportMgt.TestFileNameExtIsExcel(Filename);
                    ImportHeaderSaveFile(Data, Filename);
                    CurrPage.Update(false);
                end;
            }
        }
    }

    local procedure ImportHeaderSaveFile(Data: Text; Filename: Text)
    begin
        OnBeforeImportHeaderSaveFileImportNew(ImportHeaderIsSet, ImportHeader, SourceRecordVariantIsSet, SourceRecordVariant, OpenImportCard);
        if not ImportHeaderIsSet then
            exit;

        DropZoneImportMgt.ImportNew(ImportHeader, Data, Filename, OpenImportCard);
    end;

    procedure SetImportHeader(NewImportHeader: Record "mse365 Import Header")
    begin
        ImportHeaderIsSet := true;
        ImportHeader.Copy(NewImportHeader);
    end;

    procedure SetSourceRecordVariant(NewSourceRecordVariant: Variant)
    begin
        SourceRecordVariantIsSet := true;
        SourceRecordVariant := NewSourceRecordVariant;
    end;

    [BusinessEvent(true)]
    local procedure OnBeforeImportHeaderSaveFileImportNew(var ImportHeaderIsSet: Boolean; var ImportHeader: Record "mse365 Import Header"; SourceRecordVariantIsSet: Boolean; SourceRecordVariant: Variant; var OpenImportCard: Boolean)
    begin
    end;

    var
        ImportHeader: Record "mse365 Import Header";
        SourceRecordVariant: Variant;
        SourceRecordVariantIsSet: Boolean;
        DropZoneImportMgt: Codeunit "mse365 DropZone Import Mgt.";
        ImportHeaderIsSet: Boolean;
        OpenImportCard: Boolean;
        AddinReady: Boolean;
        MatchingIsSet: Boolean;
        CalculationHeaderIsSet: Boolean;
}
