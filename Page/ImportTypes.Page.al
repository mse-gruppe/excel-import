page 5563317 "mse365 Import Types"
{

    PageType = List;
    SourceTable = "mse365 Import Type";
    Caption = 'Import Types';
    ApplicationArea = All;
    UsageCategory = Administration;
    DelayedInsert = true;
    // DeleteAllowed = false;
    InsertAllowed = false;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field("Code"; Code)
                {
                    ApplicationArea = All;
                }
                field(Description; Description)
                {
                    ApplicationArea = All;
                }
                field("Remove First Line"; "Remove First Line")
                {
                    ApplicationArea = All;
                }
                field("Buffer Table No."; "Buffer Table No.")
                {
                    ApplicationArea = All;
                }
                field("Buffer Table Description"; "Buffer Table Description")
                {
                    ApplicationArea = All;
                }
                field("Processing Codeunit Id"; "Processing Codeunit Id")
                {
                    ApplicationArea = All;
                }
                field("Proc. Codeunit Desc."; "Proc. Codeunit Desc.")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    trigger OnOpenPage()
    begin
        InitializeImport.InitializationNotify();
    end;

    var
        InitializeImport: Codeunit "mse365 Initialize Import";
}
