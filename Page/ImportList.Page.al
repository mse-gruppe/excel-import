page 5563315 "mse365 Import List"
{
    Caption = 'mse365 Import List';
    CardPageID = "mse365 Import Card";
    Editable = false;
    PageType = List;
    SourceTable = "mse365 Import Header";
    SourceTableView = sorting("No.") order(descending);
    UsageCategory = Tasks;

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; "No.")
                {
                    ApplicationArea = All;
                }
                field("Import Type Code"; "Import Type Code")
                {
                    ApplicationArea = All;
                }
                field("Created DateTime"; "Created DateTime")
                {
                    ApplicationArea = All;
                }
                field("User ID"; "User ID")
                {
                    ApplicationArea = All;
                }
                field(Status; Status)
                {
                    ApplicationArea = All;
                }
                field("Task Scheduler Status"; "Task Scheduler Status")
                {
                    ApplicationArea = All;
                }
                field(Filename; Filename)
                {
                    ApplicationArea = All;
                }
                field("Excel Sheet Name"; "Excel Sheet Name")
                {
                    ApplicationArea = All;
                }
                field("No. Of Imported Lines"; "No. Of Imported Lines")
                {
                    ApplicationArea = All;
                }
                field("No. Of Finished Lines"; "No. Of Finished Lines")
                {
                    ApplicationArea = All;
                }
                field("No. Of Erroneous Lines"; "No. Of Erroneous Lines")
                {
                    ApplicationArea = All;
                }
            }
        }
        area(factboxes)
        {
            part(Control77015; "mse365 Setup Errors Part")
            {
                ApplicationArea = All;
                SubPageLink = "Import No." = FIELD("No.");
                SubPageView = SORTING("Import No.", "Import Line No.", "Entry No.");
            }
            systempart(Control77012; Links)
            {
                ApplicationArea = All;
            }
            systempart(Control77013; Notes)
            {
                ApplicationArea = All;
            }
        }
    }

    trigger OnOpenPage()
    begin
        InitializeImport.InitializationNotify();
    end;

    var
        InitializeImport: Codeunit "mse365 Initialize Import";
}

