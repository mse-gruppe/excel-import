page 5563316 "mse365 Import Setup"
{

    PageType = Card;
    SourceTable = "mse365 Import Setup";
    Caption = 'Import Setup';
    UsageCategory = Administration;
    InsertAllowed = false;
    DeleteAllowed = false;

    layout
    {
        area(content)
        {
            group("No. Series")
            {
                Caption = 'No. Series';
                field("Import Nos."; "Import Nos.")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(InitializeImport)
            {
                Caption = 'Initialize Import';
                ApplicationArea = All;
                Image = Indent;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                PromotedOnly = true;
                RunObject = codeunit "mse365 Initialize Import";
            }
        }
    }

    trigger OnOpenPage()
    begin
        Reset();
        if not Get() then begin
            Init();
            Insert();
        end;

        DoInitializeImport.InitializationNotify();
    end;

    var
        DoInitializeImport: Codeunit "mse365 Initialize Import";
}
