page 5563318 "mse365 Imp. Setup Errors"
{
    Caption = 'mse365 Imp. Setup Errors';
    PageType = List;
    SourceTable = "mse365 Imp. Setup Error";
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = false;

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(GetErrorText; GetErrorText())
                {
                    Caption = 'Error Text';
                    ApplicationArea = All;
                    trigger OnDrillDown()
                    begin
                        OpenPageForErrorRecord();
                    end;
                }
                field("Error Type"; "Error Type")
                {
                    ApplicationArea = All;
                }
            }

            part(ImportLines; "mse365 Import Lines")
            {
                ApplicationArea = All;
                SubPageLink = "mse365 Import No." = field("Import No."), "mse365 Import Line No." = field("Import Line No.");
            }
        }
    }
}

