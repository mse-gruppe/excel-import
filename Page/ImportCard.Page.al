page 5563312 "mse365 Import Card"
{
    Caption = 'mse365 Import';
    DeleteAllowed = false;
    SourceTable = "mse365 Import Header";

    layout
    {
        area(content)
        {
            group(Allgemein)
            {
                field("No."; "No.")
                {
                    ApplicationArea = All;
                }
                field("Import Type Code"; "Import Type Code")
                {
                    ApplicationArea = All;
                    trigger OnValidate()
                    begin
                        if "Import Type Code" <> xRec."Import Type Code" then
                            CurrPage.Update();
                    end;
                }
                field("Created DateTime"; "Created DateTime")
                {
                    ApplicationArea = All;
                }
                field("User ID"; "User ID")
                {
                    ApplicationArea = All;
                }
                field(Status; Status)
                {
                    ApplicationArea = All;
                }
                field(Filename; Filename)
                {
                    ApplicationArea = All;
                }
                field("Excel Sheet Name"; "Excel Sheet Name")
                {
                    ApplicationArea = All;
                }
                field("delete spaces"; "delete spaces")
                {
                    ApplicationArea = All;
                    trigger OnValidate()
                    begin
                        CurrPage.Update();
                    end;
                }
                field("No. Of Imported Lines"; "No. Of Imported Lines")
                {
                    ApplicationArea = All;
                }
                field("No. Of Finished Lines"; "No. Of Finished Lines")
                {
                    ApplicationArea = All;
                }
                field("No. Of Erroneous Lines"; "No. Of Erroneous Lines")
                {
                    ApplicationArea = All;
                }
                field("No. Of Warning Lines"; "No. Of Warning Lines")
                {
                    ApplicationArea = All;
                }
                field("Task Scheduler Status"; "Task Scheduler Status")
                {
                    StyleExpr = TaskStyleExpr;
                    ApplicationArea = All;
                }
                field("Task Scheduler Text"; "Task Scheduler Text")
                {
                    StyleExpr = TaskStyleExpr;
                    ApplicationArea = All;
                }
            }
            part(ImportLines; "mse365 Import Lines")
            {
                ApplicationArea = All;
                SubPageLink = "mse365 Import No." = Field("No.");
                UpdatePropagation = Both;
            }
        }
        area(factboxes)
        {
            part(DropZone; "mse365 Import DropZone Part")
            {
                ApplicationArea = All;
            }
            part(Control77025; "mse365 Setup Errors Part")
            {
                ApplicationArea = All;
                Provider = ImportLines;
                SubPageLink = "Import No." = field("mse365 Import No."), "Import Line No." = field("mse365 Import Line No.");
                SubPageView = Sorting("Import No.", "Import Line No.", "Entry No.");
            }
            systempart(Control77020; Links)
            {
                ApplicationArea = All;
            }
            systempart(Control77021; Notes)
            {
                ApplicationArea = All;
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Steps)
            {
                Caption = 'Steps';
                action("1. - Import")
                {
                    Caption = '1. - Import';
                    Image = ImportExcel;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ApplicationArea = All;

                    trigger OnAction()
                    begin
                        ImportFromExcel();
                        CurrPage.Update(false);
                    end;
                }
                action("2. - Check")
                {
                    Caption = '2. - Check';
                    Image = CheckList;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ApplicationArea = All;

                    trigger OnAction()
                    begin
                        CheckAllLines();
                        CurrPage.Update(false);
                    end;
                }
                action("3. - Transfer")
                {
                    Caption = '3. - Transfer';
                    Image = TransferToLines;
                    Promoted = true;
                    PromotedCategory = Process;
                    PromotedIsBig = true;
                    ApplicationArea = All;

                    trigger OnAction()
                    begin
                        TransferAllLines();
                        CurrPage.Update(false);
                    end;
                }
            }
            action(ResetHeader)
            {
                Caption = 'Reset';
                Image = ClearLog;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                ApplicationArea = All;

                trigger OnAction()
                begin
                    ResetHeader();
                    CurrPage.Update(false);
                end;
            }
            group(ErrorsAndWarnings)
            {
                Caption = 'Errors and Warnings';
                action(AllErrors)
                {
                    Caption = 'All Errors';
                    Image = Error;
                    RunObject = page "mse365 Imp. Setup Errors";
                    RunPageLink = "Import No." = field("No."), "Error Type" = const(Error);
                    ApplicationArea = All;
                }
                action(AllWarnings)
                {
                    Caption = 'All Warnings';
                    Image = Warning;
                    RunObject = page "mse365 Imp. Setup Errors";
                    RunPageLink = "Import No." = field("No."), "Error Type" = const(Warning);
                    ApplicationArea = All;
                }
            }
        }
    }

    trigger OnOpenPage()
    begin
        CurrPage.Editable("Task Scheduler Status" <> "Task Scheduler Status"::Running);
        CurrPage.ImportLines.Page.Editable("Task Scheduler Status" <> "Task Scheduler Status"::Running);
        InitializeImport.InitializationNotify();
    end;

    trigger OnAfterGetRecord()
    begin
        CurrPage.Editable("Task Scheduler Status" <> "Task Scheduler Status"::Running);
        CurrPage.ImportLines.Page.Editable("Task Scheduler Status" <> "Task Scheduler Status"::Running);
        TaskStyleExpr := GetStyleExpression();
        CurrPage.DropZone.Page.SetImportHeader(Rec);
    end;

    trigger OnAfterGetCurrRecord()
    begin
        CurrPage.DropZone.Page.SetImportHeader(Rec);
    end;

    local procedure GetStyleExpression(): Text
    begin
        case "Task Scheduler Status" of
            "Task Scheduler Status"::" ":
                exit('Standard');
            "Task Scheduler Status"::Running:
                exit('Ambiguous');
            "Task Scheduler Status"::Done:
                exit('Favorable');
            "Task Scheduler Status"::Error:
                exit('Unfavorable');
        end;
    end;

    var
        InitializeImport: Codeunit "mse365 Initialize Import";
        TaskStyleExpr: Text;
}

