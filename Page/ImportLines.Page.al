page 5563314 "mse365 Import Lines"
{
    Caption = 'mse365 Import Lines';
    InsertAllowed = false;
    PageType = ListPart;
    SourceTable = "mse365 Import Line";

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                Editable = "Import Status" <> "Import Status"::Finished;
                field("Import Status"; "Import Status")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("No. Of Errors"; "No. Of Errors")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("No. Of Warnings/Info"; "No. Of Warnings/Info")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 1"; "Text Value 1")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 2"; "Text Value 2")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 3"; "Text Value 3")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 4"; "Text Value 4")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 5"; "Text Value 5")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 6"; "Text Value 6")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 7"; "Text Value 7")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 8"; "Text Value 8")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 9"; "Text Value 9")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 10"; "Text Value 10")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 11"; "Text Value 11")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 12"; "Text Value 12")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 13"; "Text Value 13")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 14"; "Text Value 14")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 15"; "Text Value 15")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 16"; "Text Value 16")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 17"; "Text Value 17")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 18"; "Text Value 18")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 19"; "Text Value 19")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 20"; "Text Value 20")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 21"; "Text Value 21")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 22"; "Text Value 22")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 23"; "Text Value 23")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 24"; "Text Value 24")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 25"; "Text Value 25")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 26"; "Text Value 26")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 27"; "Text Value 27")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 28"; "Text Value 28")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 29"; "Text Value 29")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 30"; "Text Value 30")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 31"; "Text Value 31")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 32"; "Text Value 32")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 33"; "Text Value 33")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 34"; "Text Value 34")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 35"; "Text Value 35")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 36"; "Text Value 36")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 37"; "Text Value 37")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 38"; "Text Value 38")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 39"; "Text Value 39")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
                field("Text Value 40"; "Text Value 40")
                {
                    ApplicationArea = All;
                    StyleExpr = LineStyleExpression;
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Show Errors")
            {
                ApplicationArea = All;
                Caption = 'Show Errors';
                Image = Error;
                RunObject = Page "mse365 Imp. Setup Errors";
                RunPageLink = "Import No." = FIELD("mse365 Import No."),
                              "Import Line No." = FIELD("mse365 Import Line No.");
            }
            separator(Action77021)
            {
            }
            action("Show Erroneous Records")
            {
                ApplicationArea = All;
                Caption = 'Show Erroneous Records';
                Image = ErrorLog;

                trigger OnAction()
                begin
                    Reset();
                    SetRange("Import Status", "Import Status"::Error);
                    CurrPage.Update(false);
                end;
            }
            action("Show Warning Records")
            {
                ApplicationArea = All;
                Caption = 'Show Warning Records';
                Image = ShowWarning;

                trigger OnAction()
                begin
                    Reset();
                    SetFilter("No. Of Warnings/Info", '>%1', 0);
                    CurrPage.Update(false);
                end;
            }
            action("Show All Records")
            {
                ApplicationArea = All;
                Caption = 'Show All Records';
                Image = ClearFilter;

                trigger OnAction()
                begin
                    Reset();
                    SetRange("Import Status");
                    CurrPage.Update(false);
                end;
            }
        }
    }

    trigger OnAfterGetRecord()
    begin
        SetStyle();
    end;

    trigger OnAfterGetCurrRecord()
    begin
        SetStyle();
    end;

    trigger OnModifyRecord(): Boolean
    begin
        SetStyle();
    end;

    trigger OnDeleteRecord(): Boolean
    begin
        if "Import Status" = "Import Status"::Finished then
            FieldError("Import Status");

        ClearImportSetupErrors();
    end;

    local procedure EvaluateDateAndTransferToText(var DateAsText: Text): Text
    var
        Date: Date;
    begin
        Evaluate(Date, DateAsText);
        DateAsText := Format(Date);
    end;

    local procedure SetStyle()
    begin
        if "No. Of Errors" > 0 then
            LineStyleExpression := 'Attention'
        else
            LineStyleExpression := 'Standard';

        CurrPage.Update(false);
    end;

    var
        LineStyleExpression: Text;
}

