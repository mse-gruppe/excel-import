page 5563319 "mse365 Setup Errors Part"
{
    Caption = 'mse365 Setup Errors Part';
    PageType = ListPart;
    SourceTable = "mse365 Imp. Setup Error";
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = false;
    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field(GetErrorText; GetErrorText())
                {
                    ApplicationArea = All;
                    Caption = 'Error Text';
                    Width = 300;

                    trigger OnDrillDown()
                    begin
                        OpenPageForErrorRecord();
                    end;
                }
                field("Error Type"; "Error Type")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}

