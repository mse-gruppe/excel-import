report 5563312 "mse365 - Import"
{
    Caption = 'mse365 - Import';
    ProcessingOnly = true;

    dataset
    {
        dataitem("mse365 Import Header"; "mse365 Import Header")
        {
            DataItemTableView = sorting("No.");

            trigger OnAfterGetRecord()
            begin
                Import.CheckImportHeader("mse365 Import Header");
                Import.SetImportHeaderSheetAndFile("mse365 Import Header", SheetName, LocalFileName);
                Import.SetImportHeaderFileBlob("mse365 Import Header", TempBlob);

                if not RunAsBackgroundTask then begin
                    Import.ReadExcelBook(FileStream, SheetName);
                    Import.RemoveFirstLine("mse365 Import Header");
                    Import.TransferExcelBufferToImportLines("mse365 Import Header");
                    Import.SetImportHeaderToImported("mse365 Import Header");
                    "mse365 Import Header".CheckAllLines();
                end else
                    if TaskScheduler.CanCreateTask() then
                        TaskScheduler.CreateTask(Codeunit::"mse365 Import", Codeunit::"mse365 Import Failure Handler", true, '', CurrentDateTime(), "mse365 Import Header".RecordId());
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(CurrentPath; LocalFileName)
                {
                    AssistEdit = true;
                    Editable = false;
                    Caption = 'Path';
                    ApplicationArea = All;

                    trigger OnAssistEdit()
                    begin
                        LocalFileName := FileManagement.BLOBImportWithFilter(TempBlob, DialogNameLbl, '', ExcelFilterTok, ExcelExtensionTok);
                        ValidateTempBlobAndCreatFileStr();
                    end;
                }
                field(CurrentSheetName; SheetName)
                {
                    Caption = 'Worksheet Name';
                    Editable = false;
                    ApplicationArea = All;
                }
                field(BackgroundTask; RunAsBackgroundTask)
                {
                    Caption = 'Background Task';
                    ApplicationArea = All;
                }
            }
        }
    }

    local procedure ValidateTempBlobAndCreatFileStr()
    var
        ExcelBuffer: Record "Excel Buffer" temporary;
    begin
        TempBlob.Blob.CreateInStream(FileStream);
        SheetName := ExcelBuffer.SelectSheetsNameStream(FileStream);
    end;

    var
        TempBlob: Record TempBlob temporary;
        FileManagement: Codeunit "File Management";
        Import: Codeunit "mse365 Import";
        FileStream: InStream;
        SheetName: Text;
        LocalFileName: Text;
        RunAsBackgroundTask: Boolean;
        DialogNameLbl: Label 'Import mse365s';
        ExcelFilterTok: Label 'Excel File (*.xlsx)|*.xlsx';
        ExcelExtensionTok: Label 'xlsx';
}

