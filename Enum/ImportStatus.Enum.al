enum 5563313 "mse365 Import Status"
{
    Extensible = true;

    value(0; New)
    {
        Caption = 'New';
    }
    value(1; Imported)
    {
        Caption = 'Imported';
    }
    value(2; Finished)
    {
        Caption = 'Finished';
    }
}
