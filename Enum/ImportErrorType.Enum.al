enum 5563312 "mse365 Import Error Type"
{
    Extensible = true;

    value(0; Error)
    {
        Caption = 'Error';
    }
    value(1; Warning)
    {
        Caption = 'Warning';
    }
    value(2; Info)
    {
        Caption = 'Info';
    }
}
