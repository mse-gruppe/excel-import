codeunit 5563315 "mse365 Import Failure Handler"
{
    TableNo = "mse365 Import Header";

    trigger OnRun()
    begin
        SetImportedError(Rec);
    end;

    local procedure SetImportedError(var ImportHeader: Record "mse365 Import Header")
    begin
        with ImportHeader do begin
            Validate("Task Scheduler Status", "Task Scheduler Status"::Error);
            Validate("Task Scheduler Text", GetLastErrorText());
        end;
    end;
}