codeunit 5563313 "mse365 Evaluate Field Values"
{
    procedure EvaluateTextToFieldInteger(var IntVar: Integer; InputText: Text[250])
    begin
        BlankZero(InputText);
        Evaluate(IntVar, InputText);
    end;

    procedure EvaluateTextToFieldDecimal(var DecimalVar: Decimal; InputText: Text[250])
    begin
        BlankZero(InputText);
        Evaluate(DecimalVar, InputText);
    end;

    procedure EvaluateTextToFieldDate(var DateVar: Date; InputText: Text[250])
    begin
        if IsBlankValue(InputText) then
            exit;

        Evaluate(DateVar, InputText);
    end;

    procedure EvaluateTextToFieldTime(var TimeVar: Time; InputText: Text[250])
    begin
        if IsBlankValue(InputText) then
            exit;

        Evaluate(TimeVar, InputText);
    end;

    procedure EvaluateTextToFieldDateTime(DateTimeVar: DateTime; InputText: Text[250])
    begin
        if IsBlankValue(InputText) then
            exit;

        Evaluate(DateTimeVar, InputText);
    end;

    procedure EvaluateTextToFieldBoolean(var BoolVar: Boolean; InputText: Text[250])
    begin
        if IsBlankValue(InputText) then
            exit;

        Evaluate(BoolVar, InputText);
    end;

    procedure EvaluateTextToFieldCodeText(var TextVar: Text; InputText: Text[250])
    begin
        TextVar := InputText;
    end;

    local procedure BlankZero(var InputText: Text[250])
    begin
        if IsBlankValue(InputText) then
            InputText := '0';
    end;

    local procedure IsBlankValue(InputText: Text[250]): Boolean
    begin
        exit(InputText = '');
    end;
}

