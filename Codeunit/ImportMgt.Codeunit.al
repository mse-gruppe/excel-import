codeunit 5563317 "mse365 Import Mgt."
{
    TableNo = "mse365 Import Line";

    trigger OnRun()
    begin
        LineTransferFieldsToTargetTable(Rec);
    end;

    procedure HeaderImportFromExcel(var mse365ImportHeader: Record "mse365 Import Header")
    var
        mse365ImportHeaderForFilter: Record "mse365 Import Header";
    begin
        with mse365ImportHeader do begin
            TestField("Import Type Code");
            mse365ImportHeaderForFilter.Get("No.");
            mse365ImportHeaderForFilter.SetRecFilter();
            Report.RunModal(Report::"mse365 - Import", true, false, mse365ImportHeaderForFilter);
        end;
    end;

    procedure HeaderCheckAllLines(var mse365ImportHeader: Record "mse365 Import Header")
    var
        mse365ImportLine: Record "mse365 Import Line";
    begin
        with mse365ImportLine do begin
            mse365ImportHeader.TestField("Import Type Code");
            SetRange("mse365 Import No.", mse365ImportHeader."No.");
            SetRange("Import Status", "Import Status"::" ", "Import Status"::OK);

            ProgressMgt.OpenProgress(Count(), CheckingLinesLbl);
            if not FindSet() then
                exit;

            repeat
                ProgressMgt.UpdateProgress();
                Check();
            until Next() = 0;

            ProgressMgt.CloseProgress();
        end;
    end;

    procedure HeaderTransferAllLines(var mse365ImportHeader: Record "mse365 Import Header")
    var
        mse365ImportLine: Record "mse365 Import Line";
    begin
        with mse365ImportLine do begin
            mse365ImportHeader.TestField("Import Type Code");
            if mse365ImportHeader.Status = mse365ImportHeader.Status::Finished then
                Error(ImportFinishedErr);

            ConfirmFailureLines(mse365ImportHeader);

            SetRange("mse365 Import No.", mse365ImportHeader."No.");
            SetRange("Import Status", "Import Status"::Updated, "Import Status"::OK);
            ProgressMgt.OpenProgress(Count(), TransferingLinesLbl);
            if not FindSet() then
                exit;

            repeat
                ProgressMgt.UpdateProgress();
                Check();

                if "Import Status" = "Import Status"::OK then begin
                    "Check Only" := false;
                    LineTransferFieldsToTargetTable(mse365ImportLine);
                end;
            until Next() = 0;

            Commit();
            ProgressMgt.CloseProgress();
            mse365ImportHeader.UpdateStatus();
            ShowImportMsg(mse365ImportHeader);
        end;
    end;

    procedure HeaderUpdateStatus(var mse365ImportHeader: Record "mse365 Import Header")
    begin
        with mse365ImportHeader do begin
            CalcFields("No. Of Imported Lines", "No. Of Erroneous Lines", "No. Of Finished Lines");
            if ("No. Of Erroneous Lines" = 0) and ("No. Of Imported Lines" = "No. Of Finished Lines") then begin
                Status := Status::Finished;
                Modify();
            end;
        end;
    end;

    procedure HeaderDeleteRelatedEntries(var mse365ImportHeader: Record "mse365 Import Header")
    var
        mse365ImportLine: Record "mse365 Import Line";
    begin
        mse365ImportLine.SetRange("mse365 Import No.", mse365ImportHeader."No.");
        mse365ImportLine.DeleteAll(true);
    end;

    procedure LineCheck(var mse365ImportLine: Record "mse365 Import Line")
    var
        mse365ImportMgt: Codeunit "mse365 Import Mgt.";
        ImportErrorType: Enum "mse365 Import Error Type";
    begin
        DeleteOldmse365ImportErrors(mse365ImportLine);
        Commit();

        mse365ImportLine."Check Only" := true;
        if not mse365ImportMgt.Run(mse365ImportLine) then begin
            mse365ImportLine."Import Status" := mse365ImportLine."Import Status"::Error;
            mse365ImportLine.AddImportSetupError(GetLastErrorText(), 0, 0, 0, ImportErrorType::Error);
        end else
            if mse365ImportLine.HasErrors() then
                mse365ImportLine."Import Status" := mse365ImportLine."Import Status"::Error
            else
                mse365ImportLine."Import Status" := mse365ImportLine."Import Status"::OK;

        mse365ImportLine."Check Only" := false;
        mse365ImportLine.Modify();
    end;

    procedure LineValidateImportedField(var mse365ImportLine: Record "mse365 Import Line")
    begin
        with mse365ImportLine do begin
            if "Import Status" in ["Import Status"::Finished] then
                FieldError("Import Status");

            "Import Status" := "Import Status"::Updated;
            ClearImportSetupErrors();
        end;
    end;

    procedure LineGetCurrentFieldCaption(var mse365ImportLine: Record "mse365 Import Line"; ForFieldNo: Integer): Text
    var
        mse365ImportHeader: Record "mse365 Import Header";
        ImportType: Record "mse365 Import Type";
    begin
        if not mse365ImportHeader.Get(mse365ImportLine."mse365 Import No.") then
            exit('');

        if mse365ImportHeader."Import Type Code" = '' then
            exit('');

        if not ImportType.Get(mse365ImportHeader."Import Type Code") then
            exit('');

        exit(GetFieldCaption(ImportType."Buffer Table No.", ForFieldNo));
    end;

    procedure HeaderResetHeader(var ImportHeader: Record "mse365 Import Header")
    var
        ImportLine: Record "mse365 Import Line";
    begin
        with ImportHeader do begin
            CalcFields("No. Of Finished Lines");
            TestField("No. Of Finished Lines", 0);

            if not ConfirmHeaderReset() then
                exit;

            Validate(Status, Status::New);
            Modify();

            ImportLine.SetRange("mse365 Import No.", "No.");
            ImportLine.DeleteAll(true);
        end;
    end;

    local procedure GetFieldCaption(ForBufferTable: Integer; ForFieldNo: Integer): Text
    var
        FieldTable: Record Field;
    begin
        with FieldTable do begin
            SetRange(TableNo, ForBufferTable);
            SetRange("No.", ForFieldNo);
            if not FindFirst() then
                exit('');

            exit("Field Caption");
        end;
    end;

    local procedure LineTransferFieldsToTargetTable(var mse365ImportLine: Record "mse365 Import Line")
    var
        mse365ImportHeader: Record "mse365 Import Header";
        ImportType: Record "mse365 Import Type";
    begin
        with mse365ImportLine do begin
            if not mse365ImportHeader.Get(mse365ImportLine."mse365 Import No.") then
                exit;

            ImportType.Get(mse365ImportHeader."Import Type Code");
            ImportType.TestField("Processing Codeunit Id");
            Codeunit.Run(ImportType."Processing Codeunit Id", mse365ImportLine);

            if "Check Only" then
                exit;

            Validate("Import Status", "Import Status"::Finished);
            Modify();
        end;
    end;

    local procedure ConfirmFailureLines(var mse365ImportHeader: Record "mse365 Import Header")
    begin
        mse365ImportHeader.CalcFields("No. Of Erroneous Lines");
        if mse365ImportHeader."No. Of Erroneous Lines" = 0 then
            exit;

        if not Confirm(ImportConfirmationQst, false) then
            Error('');
    end;

    local procedure DeleteOldmse365ImportErrors(mse365ImportLine: Record "mse365 Import Line")
    begin
        if mse365ImportLine."mse365 Import No." = '' then
            exit;

        if mse365ImportLine."mse365 Import Line No." = 0 then
            exit;

        ContribImpSetupErrMgt.LineClearImportSetupErrors(mse365ImportLine);
    end;

    local procedure ShowImportMsg(mse365ImportHeader: Record "mse365 Import Header")
    begin
        with mse365ImportHeader do begin
            CalcFields("No. Of Imported Lines", "No. Of Finished Lines");
            if "No. Of Imported Lines" = "No. Of Finished Lines" then begin
                Message(AllRecordsImportedMsg);
                exit;
            end;

            Message(NoRecordsImportedMsg, "No. Of Finished Lines", "No. Of Imported Lines");
        end;
    end;

    local procedure ConfirmHeaderReset(): Boolean
    begin
        exit(Confirm(ConfirmHeaderResetQst, false));
    end;

    var
        ContribImpSetupErrMgt: Codeunit "mse365 Imp. Setup Err. Mgt.";
        ProgressMgt: Codeunit "mse365 Progress Mgt.";
        ImportConfirmationQst: Label 'Not all Lines are without Errors. Tranfer anyways?';
        ImportFinishedErr: Label 'The Import has already been finished.';
        AllRecordsImportedMsg: Label 'All Records have been successully imported.';
        NoRecordsImportedMsg: Label '%1 of %2 Record have been imported. Please check for Import Erros.', Comment = '%1 = Imported Records %2 = All Records';
        ConfirmHeaderResetQst: Label 'Reset Import?';
        CheckingLinesLbl: Label 'Checking Lines';
        TransferingLinesLbl: Label 'Transfering Lines';
}

