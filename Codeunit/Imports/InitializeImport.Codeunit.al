codeunit 5563320 "mse365 Initialize Import"
{
    trigger OnRun()
    begin
        InitImport();
    end;

    procedure InitImport(SetupNotification: Notification)
    begin
        InitImport();
    end;

    procedure InitImport()
    begin
        OnBeforeInitImport();
    end;

    procedure InitializationNotify()
    var
        SetupNotification: Notification;
    begin
        if not ImportType.IsEmpty() then
            exit;

        SetupNotification.Message(ImportNotInitializedLbl);
        SetupNotification.Scope := NotificationScope::LocalScope;
        SetupNotification.AddAction(InitializedActionLbl, Codeunit::"mse365 Initialize Import", 'InitImport');
        SetupNotification.Send();
    end;

    [BusinessEvent(false)]
    local procedure OnBeforeInitImport()
    begin
    end;

    var
        ImportType: Record "mse365 Import Type";
        ImportNotInitializedLbl: Label 'The Import has not been initialized.';
        InitializedActionLbl: Label 'Initialize Import?';
}