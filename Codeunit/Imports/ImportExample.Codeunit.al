//TODO Create Import Codeunit
/*
codeunit 5563321 "mse365 Import Example"
{
    TableNo = "Import Line";

    trigger OnRun()
    begin
        LineTransferFields(Rec);
    end;

    procedure LineTransferFields(var ImportLine: Record " Import Line")
    var
        PartsBuffer: Record " Parts Buffer";
    begin
        EvaluateFieldValues(PartsBuffer, ImportLine);
        CreateUpdatePart(ImportLine, PartsBuffer);
    end;

    local procedure EvaluateFieldValues(var PartsBuffer: Record " Parts Buffer"; ImportLine: Record " Import Line")
    begin
        with PartsBuffer do begin
            Evaluate("Part No.", ImportLine."Text Value 1");
            EvaluateFieldValues.EvaluateTextToFieldDecimal("Price EUR", ImportLine."Text Value 2");
            EvaluateFieldValues.EvaluateTextToFieldDecimal("Core Price EUR", ImportLine."Text Value 3");
        end;
    end;

    local procedure CreateUpdatePart(ImportLine: Record " Import Line"; PartsBuffer: Record " Parts Buffer")
    var
        Part: Record " Part";
        UnitOfMeasure: Record "Unit of Measure";
    begin
        with Part do begin
            if not Get(PartsBuffer."Part No.") then begin
                Init();
                Validate("No.", PartsBuffer."Part No.");
                if not ImportLine."Check Only" then
                    Insert();
            end;

            if PartsBuffer."Base Unit of Measure" <> '' then
                UnitOfMeasure.Get(PartsBuffer."Base Unit of Measure");

            Validate("Unit Price", PartsBuffer."Price EUR");
            Validate("Core Price", PartsBuffer."Core Price EUR");
            Validate(Description, PartsBuffer."Designation (german)");
            Validate("Base Unit of Measurement", PartsBuffer."Base Unit of Measure");
            Validate("Custom Assigned No.", PartsBuffer."CAN (Customs assigned number)");
            Validate(Measurements, PartsBuffer.Dimensions);
            Validate(Norm, PartsBuffer.Norm);
            Validate(Weight, PartsBuffer."Unit Weight");
            Validate("Unit of Weight", PartsBuffer."Unit of Weight");
            Validate(PRDH, PartsBuffer.PRDH);
            Validate("Nato Version No.", PartsBuffer."NATO-VERS.-NR.");
            Validate("Cold UB (Days)", PartsBuffer."COLT UB (days)");
            Validate("Effective Date", PartsBuffer."Effective Date");

            if ImportLine."Check Only" then
                exit;

            if not Insert(true) then
                Modify(true);
        end;
    end;

    var
        EvaluateFieldValues: Codeunit " Evaluate Field Values";
}
*/