codeunit 5563312 "mse365 DropZone Import Mgt."
{
    procedure ImportNew(var ImportHeader: Record "mse365 Import Header"; Data: Text; Filename: Text; OpenImportCard: Boolean)
    var
        Content: Text;
    begin
        ImportHeader.TestField(Status, ImportHeader.Status::New);

        if not GetContent(Data, Content) then
            exit;

        ReadAndImportFile(ImportHeader, Filename, Content);

        if OpenImportCard then
            Page.Run(Page::"mse365 Import Card", ImportHeader);
    end;

    procedure TestFileNameExtIsExcel(var Filename: Text)
    var
        FileNameUpperCase: Text;
    begin
        FileNameUpperCase := UpperCase(Filename);
        if FileNameUpperCase.Contains('.XLS') then
            exit;

        Error(InvalidFileExtensionErr);
    end;

    local procedure GetContent(var Data: Text; var Content: Text): Boolean
    begin
        if Data = '' then
            exit(false);

        Content := CopyStr(Data, StrPos(Data, 'base64,') + StrLen('base64,'), StrLen(Data));
        if Content = '' then
            exit(false);

        exit(true);
    end;

    local procedure ReadAndImportFile(var ImportHeader: Record "mse365 Import Header"; var Filename: Text; var Content: Text)
    var
        TempBlob: Record TempBlob temporary;
        FileInSteam: InStream;
        SheetName: Text;
    begin
        TempBlob.Init();
        TempBlob.FromBase64String(Content);

        TempBlob.Blob.CreateInStream(FileInSteam);
        SheetName := ExcelBuffer.SelectSheetsNameStream(FileInSteam);

        Import.SetImportHeaderSheetAndFile(ImportHeader, SheetName, Filename);
        Import.SetImportHeaderFileBlob(ImportHeader, TempBlob);
        Import.ReadExcelBook(FileInSteam, SheetName);
        Import.RemoveFirstLine(ImportHeader);
        Import.TransferExcelBufferToImportLines(ImportHeader);
        Import.SetImportHeaderToImported(ImportHeader);
        ImportHeader.CheckAllLines();
    end;

    var
        ExcelBuffer: Record "Excel Buffer";
        Import: Codeunit "mse365 Import";
        InvalidFileExtensionErr: Label 'Invalid File Extension. Please select an Excel-File.';
}