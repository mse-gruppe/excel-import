codeunit 5563318 "mse365 Imp. Setup Err. Mgt."
{
    procedure GetDataCaptionExpression(var mse365ImpSetupError: Record "mse365 Imp. Setup Error"): Text
    begin
        with mse365ImpSetupError do
            exit(StrSubstNo('%1 - %2', "Import No.", "Import Line No."));
    end;

    procedure GetNextEntryNo(var mse365ImpSetupError: Record "mse365 Imp. Setup Error"): Integer
    var
        mse365ImpSetupErrorForSearch: Record "mse365 Imp. Setup Error";
    begin
        with mse365ImpSetupError do begin
            mse365ImpSetupErrorForSearch.SetRange("Import No.", "Import No.");
            mse365ImpSetupErrorForSearch.SetRange("Import Line No.", "Import Line No.");
            if not mse365ImpSetupErrorForSearch.FindLast() then
                exit(1);

            exit(mse365ImpSetupErrorForSearch."Entry No." + 1);
        end;
    end;

    procedure SetErrorText(var mse365ImpSetupError: Record "mse365 Imp. Setup Error"; NewErrorText: Text)
    begin
        with mse365ImpSetupError do begin
            Clear("Error Text 1");
            Clear("Error Text 2");
            Clear("Error Text 3");
            Clear("Error Text 4");

            "Error Text 1" := CopyStr(NewErrorText, 1, 250);
            "Error Text 2" := CopyStr(NewErrorText, 251, 250);
            "Error Text 3" := CopyStr(NewErrorText, 501, 250);
            "Error Text 4" := CopyStr(NewErrorText, 751, 250);
        end;
    end;

    procedure GetErrorText(var mse365ImpSetupError: Record "mse365 Imp. Setup Error"): Text
    begin
        with mse365ImpSetupError do
            exit("Error Text 1" + "Error Text 2" + "Error Text 3" + "Error Text 4");
    end;

    procedure OpenPageForErrorRecord(var mse365ImpSetupError: Record "mse365 Imp. Setup Error")
    var
        ErrorRecRef: RecordRef;
        ErrorRec: Variant;
    begin
        with mse365ImpSetupError do begin
            if not ErrorRecRef.Get("Error RecordID") then
                exit;

            ErrorRecRef.SetRecFilter();

            ErrorRec := ErrorRecRef;
            PAGE.Run("Page ID", ErrorRec);
        end;
    end;

    procedure LineAddImportSetupError(var mse365ImportLine: Record "mse365 Import Line"; ErrorText: Text; ErrorRecID: Variant; PageID: Integer; ErrorCode: Integer; ImportErrorType: Enum "mse365 Import Error Type")
    var
        mse365ImpSetupError: Record "mse365 Imp. Setup Error";
        RecRef: RecordRef;
        NextEntryNo: Integer;
        ValidRecID: Boolean;
    begin
        with mse365ImportLine do begin
            ValidRecID := false;
            if ErrorRecID.IsRecord then begin
                ValidRecID := true;
                RecRef.GetTable(ErrorRecID);
            end else
                if ErrorRecID.IsRecordRef then begin
                    ValidRecID := true;
                    RecRef := ErrorRecID;
                end;

            NextEntryNo := 1;

            mse365ImpSetupError.Reset();
            mse365ImpSetupError.SetRange("Import No.", "mse365 Import No.");
            mse365ImpSetupError.SetRange("Import Line No.", "mse365 Import Line No.");
            if mse365ImpSetupError.FindLast() then
                NextEntryNo := mse365ImpSetupError."Entry No." + 1;

            mse365ImpSetupError.Reset();
            mse365ImpSetupError.Init();
            mse365ImpSetupError."Import No." := "mse365 Import No.";
            mse365ImpSetupError."Import Line No." := "mse365 Import Line No.";
            mse365ImpSetupError."Entry No." := NextEntryNo;
            if ValidRecID then
                mse365ImpSetupError."Error RecordID" := RecRef.RecordId;
            mse365ImpSetupError."Page ID" := PageID;
            mse365ImpSetupError.SetErrorText(ErrorText);
            mse365ImpSetupError."Error Code" := ErrorCode;
            mse365ImpSetupError."Error Type" := ImportErrorType;
            mse365ImpSetupError.Insert();
        end;
    end;

    procedure LineClearImportSetupErrors(var mse365ImportLine: Record "mse365 Import Line")
    var
        mse365ImpSetupError: Record "mse365 Imp. Setup Error";
    begin
        with mse365ImportLine do begin
            mse365ImpSetupError.Reset();
            mse365ImpSetupError.SetRange("Import No.", "mse365 Import No.");
            mse365ImpSetupError.SetRange("Import Line No.", "mse365 Import Line No.");
            mse365ImpSetupError.DeleteAll();
        end;
    end;

    procedure LineHasErrors(var mse365ImportLine: Record "mse365 Import Line"): Boolean
    begin
        with mse365ImportLine do begin
            CalcFields("No. Of Errors");
            exit("No. Of Errors" <> 0);
        end;
    end;
}

