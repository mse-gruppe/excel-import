codeunit 5563316 "mse365 Import Install"
{
    Subtype = Upgrade;

    trigger OnRun()
    begin
        if Codeunit.Run(Codeunit::"mse365 Initialize Import") then;
    end;
}