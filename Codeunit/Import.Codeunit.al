codeunit 5563314 "mse365 Import"
{
    TableNo = "mse365 Import Header";

    trigger OnRun()
    var
        FileStream: InStream;
    begin
        SetTaskRunning(Rec);
        Rec.CalcFields("File Blob");
        Rec."File Blob".CreateInStream(FileStream);
        ReadExcelBook(FileStream, Rec."Excel Sheet Name");
        RemoveFirstLine(Rec);
        TransferExcelBufferToImportLines(Rec);
        SetImportHeaderToImported(Rec);
        SetTaskFinished(Rec);
        Rec.CheckAllLines();
    end;

    procedure CheckImportHeader(var ImportHeader: Record "mse365 Import Header")
    begin
        ImportHeader.TestField(Status, ImportHeader.Status::New);
    end;

    procedure SetImportHeaderSheetAndFile(var ImportHeader: Record "mse365 Import Header"; SheetName: Text; Filename: Text)
    begin
        ImportHeader."Excel Sheet Name" := SheetName;
        ImportHeader.Filename := Filename;
        ImportHeader.Modify();
    end;

    procedure SetImportHeaderFileBlob(var ImportHeader: Record "mse365 Import Header"; var TempBlob: Record TempBlob temporary)
    begin
        Clear(ImportHeader."File Blob");
        ImportHeader."File Blob" := TempBlob.Blob;
        ImportHeader.Modify();
    end;

    procedure SetImportHeaderToImported(var ImportHeader: Record "mse365 Import Header")
    begin
        ImportHeader.Status := ImportHeader.Status::Imported;
        ImportHeader.Modify();
    end;

    procedure ReadExcelBook(FileStream: InStream; SheetName: Text)
    begin
        ExcelBuffer.Reset();
        ExcelBuffer.DeleteAll();

        ExcelBuffer.OpenBookStream(FileStream, SheetName);
        ExcelBuffer.ReadSheet();
    end;

    procedure TransferExcelBufferToImportLines(var mse365ImportHeader: Record "mse365 Import Header")
    var
        mse365IMportLine: Record "mse365 Import Line";
    begin
        NextEntryNo := 1;

        ExcelBuffer.Reset();
        ExcelBuffer.SetRange("Column No.", 1, 40);
        if not ExcelBuffer.FindSet() then
            exit;

        repeat
            TransferExcelBufferToImportLine(ExcelBuffer, mse365ImportHeader, mse365IMportLine, NextEntryNo);
        until ExcelBuffer.Next() = 0;
    end;

    local procedure TransferExcelBufferToImportLine(var ExcelBuffer: Record "Excel Buffer"; var mse365ImportHeader: Record "mse365 Import Header"; var mse365IMportLine: Record "mse365 Import Line"; var NextEntryNo: Integer)
    begin
        with mse365IMportLine do begin
            InitInsertNewImportLine(ExcelBuffer, mse365ImportHeader, mse365IMportLine, NextEntryNo);

            case ExcelBuffer."Column No." of
                1:
                    begin
                        "Text Value 1" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 1" := DelChr("Text Value 1", '=');
                        Modify();
                    end;
                2:
                    begin
                        "Text Value 2" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 2" := DelChr("Text Value 2", '=');
                        Modify();
                    end;

                3:
                    begin
                        "Text Value 3" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 3" := DelChr("Text Value 3", '=');
                        Modify();
                    end;
                4:
                    begin
                        "Text Value 4" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 4" := DelChr("Text Value 4", '=');
                        Modify();
                    end;
                5:
                    begin
                        "Text Value 5" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 5" := DelChr("Text Value 5", '=');
                        Modify();
                    end;
                6:
                    begin
                        "Text Value 6" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 6" := DelChr("Text Value 6", '=');
                        Modify();
                    end;
                7:
                    begin
                        "Text Value 7" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 7" := DelChr("Text Value 7", '=');
                        Modify();
                    end;
                8:
                    begin
                        "Text Value 8" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 8" := DelChr("Text Value 8", '=');
                        Modify();
                    end;
                9:
                    begin
                        "Text Value 9" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 9" := DelChr("Text Value 9", '=');
                        Modify();
                    end;
                10:
                    begin
                        "Text Value 10" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 10" := DelChr("Text Value 10", '=');
                        Modify();
                    end;
                11:
                    begin
                        "Text Value 11" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 11" := DelChr("Text Value 11", '=');
                        Modify();
                    end;
                12:
                    begin
                        "Text Value 12" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 12" := DelChr("Text Value 12", '=');
                        Modify();
                    end;
                13:
                    begin
                        "Text Value 13" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 13" := DelChr("Text Value 13", '=');
                        Modify();
                    end;
                14:
                    begin
                        "Text Value 14" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 14" := DelChr("Text Value 14", '=');
                        Modify();
                    end;
                15:
                    begin
                        "Text Value 15" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 15" := DelChr("Text Value 15", '=');
                        Modify();
                    end;
                16:
                    begin
                        "Text Value 16" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 16" := DelChr("Text Value 16", '=');
                        Modify();
                    end;
                17:
                    begin
                        "Text Value 17" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 17" := DelChr("Text Value 17", '=');
                        Modify();
                    end;
                18:
                    begin
                        "Text Value 18" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 18" := DelChr("Text Value 18", '=');
                        Modify();
                    end;
                19:
                    begin
                        "Text Value 19" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 19" := DelChr("Text Value 19", '=');
                        Modify();
                    end;
                20:
                    begin
                        "Text Value 20" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 20" := DelChr("Text Value 20", '=');
                        Modify();
                    end;
                21:
                    begin
                        "Text Value 21" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 21" := DelChr("Text Value 21", '=');
                        Modify();
                    end;
                22:
                    begin
                        "Text Value 22" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 22" := DelChr("Text Value 22", '=');
                        Modify();
                    end;
                23:
                    begin
                        "Text Value 23" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 23" := DelChr("Text Value 23", '=');
                        Modify();
                    end;
                24:
                    begin
                        "Text Value 24" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 24" := DelChr("Text Value 24", '=');
                        Modify();
                    end;
                25:
                    begin
                        "Text Value 25" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 25" := DelChr("Text Value 25", '=');
                        Modify();
                    end;
                26:
                    begin
                        "Text Value 26" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 26" := DelChr("Text Value 26", '=');
                        Modify();
                    end;
                27:
                    begin
                        "Text Value 27" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 27" := DelChr("Text Value 27", '=');
                        Modify();
                    end;
                28:
                    begin
                        "Text Value 28" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 28" := DelChr("Text Value 28", '=');
                        Modify();
                    end;
                29:
                    begin
                        "Text Value 29" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 29" := DelChr("Text Value 29", '=');
                        Modify();
                    end;
                30:
                    begin
                        "Text Value 30" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 30" := DelChr("Text Value 30", '=');
                        Modify();
                    end;
                31:
                    begin
                        "Text Value 31" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 31" := DelChr("Text Value 31", '=');
                        Modify();
                    end;
                32:
                    begin
                        "Text Value 32" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 32" := DelChr("Text Value 32", '=');
                        Modify();
                    end;
                33:
                    begin
                        "Text Value 33" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 33" := DelChr("Text Value 33", '=');
                        Modify();
                    end;
                34:
                    begin
                        "Text Value 34" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 34" := DelChr("Text Value 34", '=');
                        Modify();
                    end;
                35:
                    begin
                        "Text Value 35" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 35" := DelChr("Text Value 35", '=');
                        Modify();
                    end;
                36:
                    begin
                        "Text Value 36" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 36" := DelChr("Text Value 36", '=');
                        Modify();
                    end;
                37:
                    begin
                        "Text Value 37" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 37" := DelChr("Text Value 37", '=');
                        Modify();
                    end;
                38:
                    begin
                        "Text Value 38" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 38" := DelChr("Text Value 38", '=');
                        Modify();
                    end;
                39:
                    begin
                        "Text Value 39" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 39" := DelChr("Text Value 39", '=');
                        Modify();
                    end;
                40:
                    begin
                        "Text Value 40" := ExcelBuffer."Cell Value as Text";
                        if mse365ImportHeader."delete spaces" then
                            "Text Value 40" := DelChr("Text Value 40", '=');
                        Modify();
                    end;
            end;
        end;
    end;

    local procedure SetTaskFinished(var ImportHeader: Record "mse365 Import Header")
    begin
        ImportHeader.Validate("Task Scheduler Status", ImportHeader."Task Scheduler Status"::Done);
        ImportHeader.Validate("Task Scheduler Text", ImportedWithoutErrorsLbl);
        ImportHeader.Modify();
    end;

    local procedure SetTaskRunning(var ImportHeader: Record "mse365 Import Header")
    begin
        ImportHeader.Validate("Task Scheduler Status", ImportHeader."Task Scheduler Status"::Running);
        ImportHeader.Validate("Task Scheduler Text", '');
        ImportHeader.Modify();
    end;

    local procedure InitInsertNewImportLine(var ExcelBuffer: Record "Excel Buffer"; var ImportHeader: Record "mse365 Import Header"; var ImportLine: Record "mse365 Import Line"; var NextEntryNo: Integer)
    begin
        if (LastRowNo <> 0) and (LastRowNo = ExcelBuffer."Row No.") then
            exit;

        ImportLine.Init();
        ImportLine."mse365 Import No." := ImportHeader."No.";
        ImportLine."mse365 Import Line No." := NextEntryNo;
        ImportLine.Insert();

        NextEntryNo += 1;
        LastRowNo := ExcelBuffer."Row No.";
    end;

    procedure RemoveFirstLine(ImportHeader: Record "mse365 Import Header")
    var
        ImportType: Record "mse365 Import Type";
    begin
        ImportType.Get(ImportHeader."Import Type Code");
        if not ImportType."Remove First Line" then
            exit;

        ExcelBuffer.SetRange("Row No.", 1);
        ExcelBuffer.DeleteAll();
    end;

    var
        ExcelBuffer: Record "Excel Buffer" temporary;
        NextEntryNo: Integer;
        ImportedWithoutErrorsLbl: Label 'Imported without errors';
        LastRowNo: Integer;
}