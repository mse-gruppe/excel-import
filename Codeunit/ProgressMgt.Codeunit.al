codeunit 5563319 "mse365 Progress Mgt."
{
    procedure OpenProgress(NewTotalCount: Integer; NewCurrentStep: Text)
    begin
        if NewTotalCount = 0 then
            exit;

        TotalCount := NewTotalCount;
        CurrentStep := NewCurrentStep;
        OpenProgress();
    end;

    local procedure OpenProgress(NewTotalCount: Integer)
    begin
        TotalCount := NewTotalCount;
        OpenProgress();
    end;

    procedure OpenProgress()
    begin
        if not GuiAllowed() then
            exit;

        Clear(CurrentCount);
        CalculateFactor();
        if CurrentStep = '' then
            Progress.Open(ProgressLbl)
        else
            Progress.Open(ProgressWithStepLbl);
    end;

    procedure UpdateProgress()
    begin
        if not GuiAllowed() then
            exit;

        CurrentCount += 1;
        Progress.Update(1, (CurrentCount * Factor) div 1);
        if CurrentStep <> '' then
            Progress.Update(2, CurrentStep);
    end;

    procedure CloseProgress()
    begin
        if not GuiAllowed() then
            exit;

        Progress.Close();
    end;

    procedure SetTotalCount(NewTotalCount: Integer)
    begin
        TotalCount := NewTotalCount;
        CalculateFactor();
    end;

    procedure SetCurrentStep(NewCurrentStep: Text)
    begin
        CurrentStep := NewCurrentStep;
    end;

    local procedure CalculateFactor()
    begin
        if TotalCount = 0 then
            exit;

        Factor := 9999 / TotalCount;
    end;

    var
        Progress: Dialog;
        TotalCount: Integer;
        Factor: Decimal;
        CurrentCount: Integer;
        CurrentStep: Text;
        ProgressLbl: Label 'Processing...\\Progress:@1@@@@@@@@@@';
        ProgressWithStepLbl: Label 'Processing...\\Progress:@1@@@@@@@@@@\Current Step:#2##########';
}