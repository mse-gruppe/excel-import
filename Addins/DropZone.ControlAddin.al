controladdin "mse365 DropZone"
{
    StyleSheets = 'Addins/DropZone/dist/DragDrop.css',
                'Addins/DropZone/CssIcons/fileicon.css';
    StartupScript = 'Addins/DropZone/start.js';
    Scripts = 'Addins/DropZone/dist/JQuery.js',
              'Addins/DropZone/dist/DragDrop.js',
              'Addins/DropZone/MsgReader/DataStream.js',
              'Addins/DropZone/MsgReader/msg.reader.js',
              'Addins/DropZone/MsgReader/multipart.form.js';

    RequestedHeight = 215;
    RequestedWidth = 300;
    VerticalStretch = true;
    HorizontalStretch = true;

    event ControlAddInReady();
    event DataReceived(Data: text; Filename: text);
    event EMailDataReceived(Data: text; Filename: text; EMailDataJson: JsonObject);

    procedure showExcelIcon();
    procedure showRandomDocTypeIcon();
}