﻿var navControlHolder;
var navControl;
var IconBackgroundColor;
var DragIconBackgroundColor;

function CreateControl() {
    navControlHolder = $("#controlAddIn");
    navControlHolder.append('<div id="drop-files"><div id="drop-image"></div></div>');
    navControl = $("#drop-files");
    
    jQuery.event.props.push('dataTransfer');
    
    navControl.bind('drop', function (e) {
        var files = e.originalEvent.dataTransfer.files;
        $.each(files, function (index, file) {
            var fileReader = new FileReader();
            var filename = file.name;
           
            fileReader.onload = (function (file) {
                var arrayBuffer = fileReader.result;
                
                if (filename.indexOf('.msg') !== -1){
                    var msgReader = new MSGReader(Base64ToArrayBuffer(arrayBuffer));
                    var fileData = msgReader.getFileData();

                    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("EMailDataReceived", [arrayBuffer, filename, fileData]);
                }
                else {
                    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod("DataReceived", [arrayBuffer, filename]);
                }
            });

            fileReader.readAsDataURL(file);
        });
    });

    function Base64ToArrayBuffer(base64) {
        var commaPos = base64.indexOf(',');
        if (commaPos > 0) base64 = base64.slice(commaPos + 1);
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) bytes[i] = binary_string.charCodeAt(i);
        return bytes.buffer;
    }

    navControl.bind('dragover', function (e) {
        e.dataTransfer.dragEffect = "copy";
        $(this).addClass('file-drag');
        document.getElementById('icon').style.backgroundColor = DragIconBackgroundColor;
        
        return false;
    });
    navControl.bind('drop', function () {
        $(this).removeClass('file-drag');
        document.getElementById('icon').style.backgroundColor = IconBackgroundColor;
        return false;
    });
    navControl.bind('dragleave', function () {
        $(this).removeClass('file-drag');
        document.getElementById('icon').style.backgroundColor = IconBackgroundColor;
        return false;
    });
}

function showExcelIcon() {
    IconHolder = $("#drop-image");
    IconHolder.append('<div id="icon" class="file-icon file-icon-xl" datatype="xls"></div>');
    IconBackgroundColor = document.getElementById('icon').style.backgroundColor;
    DragIconBackgroundColor = "#0b8c58";
}

function showRandomDocTypeIcon() {
    IconHolder = $("#drop-image");
    IconHolder.append('<div id="icon" class="file-icon file-icon-xl" datatype=""></div>');
    IconBackgroundColor = document.getElementById('icon').style.backgroundColor;
    DragIconBackgroundColor = "#2d69f7";
  }
